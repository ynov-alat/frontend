import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Stripe } from 'stripe-angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-undercover',
  styleUrls: ['./undercover.component.css'],
  templateUrl: './undercover.component.html',
})
export class UndercoverComponent implements OnInit {

  private isGoToShop = false;
  public isLogin = false;
  private url = `http://localhost:5000/`;
  public errorMessage: any;
  public stripe = Stripe('pk_test_51ICMVxHmOoJeDnTyyTlROjI2TYpe5JPpmeweyoOKJectQZqQevrYawhcfzOvTDScfcU1vlGDLmj0z3N68lcclW6100TpCBQNpH');

  constructor(private http: HttpClient, private authService: AuthService, private readonly route: ActivatedRoute) {
    this.isGoToShop = route.snapshot.data.goToShop;
  }

  public ngOnInit(): void {
    if (this.isGoToShop) {
      this.http.get(this.url + 'auth/check-token').subscribe({
        next: (response: any) => {
          this.isLogin = true;
          this.authService.setDataInLocalStorage('id', response.id);
          this.authService.setDataInLocalStorage('username', response.username);
          this.createCheckoutSession();
        },
        error: (error) => {
          this.isLogin = false;
        },
      });
    }
  }

  isPopupOpen: boolean = false;

  openPopup() {
    this.isPopupOpen = true;
  }

  closePopup() {
    this.isPopupOpen = false;
  }

  goToShop() {
    this.openPopup();
    if (this.isLogin) {
      this.createCheckoutSession()
    }
  }

  public onSubmit(username: string, password: string) {
    this.http.post(this.url + 'auth/login', { username, password }).subscribe({
      next: (res: any) => {
        console.log(res);
        this.isLogin = true;
        this.authService.setDataInLocalStorage('id', res.id);
        this.authService.setDataInLocalStorage('username', res.username);
        this.authService.setDataInLocalStorage('token', res.token);
        this.createCheckoutSession();
      },
      error: (err: any) => {
        console.log(err);

      },
    });
  }

  public createCheckoutSession() {
    this.http.post(this.url + 'shop/create-checkout-session', { id: localStorage.getItem('id') }).subscribe({
      next: (res: any) => {
        return this.stripe.redirectToCheckout({ sessionId: res.id }).then(response => {
          console.log(response);
        }).catch(error => {
          console.log(error);
        });
      },
      error: (err: any) => {
        console.log(err);

      },
    });
  }

}
