import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor() { }

  getUserDetails() {
    // const userData = localStorage.getItem('userData');
    // if(userData) {
    //   console.log(userData);
    //   return JSON.parse(userData);
    // }
    const username = localStorage.getItem('username');
    if (username) {
      console.log(username);
      return username;
    }
    else {
      console.log('casser');
      return null;
    }
    // else if (userToken) {
    //  console.log(userData);
    // return JSON.parse(userToken);
  }

  setDataInLocalStorage(variableName: string, data: string) {
    localStorage.setItem(variableName, data);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  clearStorage() {
    localStorage.clear();
  }
}
