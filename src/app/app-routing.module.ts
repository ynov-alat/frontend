import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UndercoverComponent } from './components/undercover/undercover.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: 'shop/:token', canActivate: [AuthGuard], data: { goToShop: true }, component: UndercoverComponent },
  { path: 'shop', component: UndercoverComponent },
  { path: '**', redirectTo: 'shop', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
