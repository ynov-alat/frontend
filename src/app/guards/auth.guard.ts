import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';

/** Guards to check the connexion of the user */
@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(private authService: AuthService, private router: Router, private location: Location) { }

    private async setToken(token: string): Promise<boolean> {
        this.authService.setDataInLocalStorage('token', token);
        return true;
    }
    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.setToken(route.params.token);
    }
}
